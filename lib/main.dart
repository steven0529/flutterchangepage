import 'package:MyFirstFlutter/second.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Change Page',
        theme: ThemeData(
          primarySwatch: Colors.green,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text('換頁Demo'),
          ),
          body: _FirstPage(),
        ),
        routes: <String, WidgetBuilder>{'/second': (_) => new SecondPage()});
  }
}

class _FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: RaisedButton(
          child: Text("去第二頁"),
          onPressed: () {
            Navigator.pushNamed(context, "/second");
          }),
    );
  }
}
